# What is new in Ruby 2.5

- A big part of what makes Ruby nice is its ease-of-use, its rich and convenient features.
- No huge changes in 2.5, but there are a few significant things.
- Is currently in a preview1 release.

## rescue/else/ensure are allowed inside do/end blocks without begin/end

*Syntax change!*
Not only does this look nice, but it matches the syntax for methods. Unfortunately, since this is a new syntax, it cannot live in a file which will be read by and older Ruby interpreter.

## Top-level constant lookup is removed

The problem is that constant lookup in a _class_ could return an unexpected result.
This fix is good because it adds an element of safety!
In theory this shouldn't break any code which wasn't already broken.
More importantly, just don't use classes as namespaces.

## Bundler gem is now bundled

While this is cool, it can break scripts which auto-install gems. `asdf` hangs when trying to auto-install bundler.

## Print backtrace and error message in reverse order (experimental)

Not sure why this is being done. Keep in mind it is experimental. If this is does make it into Ruby 2.5 (which I think is iffy), then hopefully it is behind a flag. This feature only takes effect when STDERR is unchanged and a tty.

## Kernel#yield_self

The use for this seems pretty limited. Drop in replacement: collection/map -> object/yield_self. I.e. `yield_self` is to Object as `map` is to `Array`.

## String#delete_prefix/delete_suffix

Much faster than equivalent `sub` with regex anchors.

## Array#prepend/append as aliases of unshift/push

Cool story, bro.

## Hash#transform_keys/transform_keys!

Maps a hashes keys from their current values to some transformed values. Counterpart to Hash#transform_values.
Hash#symbolize_keys/stringify_keys (from ActiveSupport) preceded this.

## Dir.children/each_child

I'm sure sysadmins, or anyone who does a lot of file system work, like this.

## ERB#result_with_hash

Works like `render :locals`.

## Integer

Integer#{round,floor,ceil,truncate} now always return an Integer.

# Other significant changes since 2.4.0

## Other

RDoc performance improvement, new lexer.
Upgraded regex engine, includes absent operator.
Low-level File API changes.
(most important???) Lots of specs added! (as in RSpec)

# References

- https://blog.jetbrains.com/ruby/2017/10/10-new-features-in-ruby-2-5/
- https://www.ruby-lang.org/en/news/2017/10/10/ruby-2-5-0-preview1-released/
- https://github.com/ruby/ruby/blob/v2_5_0_preview1/NEWS
