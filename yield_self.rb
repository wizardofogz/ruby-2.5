# ruby yield_self.rb

# frozen_string_literal: true

n = 5
result = n.yield_self do |x|
  x ** x
end

puts result

#---------------------------------------------------------------------------------------------------

# numbers = [2, 3, 5, 7, 11, 13, 17, 19]
#
# puts "Using Array#map:"
# puts numbers
#   .map(&:to_f)                      # 2.0
#   .map(&:to_s)                      # "2.0"
#   .map(&:reverse)                   # "0.2"
#   .map { |s| "Backwards: #{ s }" }  # "Backwards: 0.2"
#   .map(&:upcase)                    # "BACKWARDS: 0.2"
#
# puts "\nUsing Object#yield_self (corresponding to map)"
# puts 23
#   .yield_self(&:to_f)
#   .yield_self(&:to_s)
#   .yield_self(&:reverse)
#   .yield_self { |s| "Backwards: #{ s }" }
#   .yield_self(&:upcase)
#
# puts "\nUsing Object#yield_self (simplified case)"
# puts 29
#   .to_f
#   .to_s
#   .reverse
#   .yield_self { |s| "Backwards: #{ s }" }
#   .upcase

#---------------------------------------------------------------------------------------------------

# require 'benchmark/ips'
#
# class Foo
#   attr_accessor :a, :b
# end
#
# Benchmark.ips do |x|
#   foo = Foo.new
#   foo.a = 13
#   foo.b = 17
#
#   x.report("yield_self") { foo.yield_self { |o| o.a * o.b } }
#   x.report("instance_eval") { foo.instance_eval { a * b } }    # context is different
#
#   x.compare!
# end
