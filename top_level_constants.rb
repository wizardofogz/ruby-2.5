# ASDF_RUBY_VERSION=2.4.2 ruby --disable-did_you_mean top_level_constants.rb
# ruby top_level_constants.rb

# frozen_string_literal: true

# Constants defined in the root (Object) namespace.
class FooClass; end
module BarModule; end

begin
  FooClass::BarModule
  # (pry):3: warning: toplevel constant FooClass referenced by FooClass::BarModule
  # => BarModule
rescue NameError => e
  puts "NameError: #{ e.message }"
end
puts "FooClass.ancestors: #{ FooClass.ancestors.inspect }\n\n"
# puts "Object.constants: #{ Object.constants.grep(/Foo|Bar/).inspect }"

#---------------------------------------------------------------------------------------------------

# ### Lookup "works" differently with modules (just because Object is not in the module ancestry).
# module MyModule; end
# module OtherModule; end
#
# begin
#   OtherModule::MyModule
#   # NameError: uninitialized constant OtherModule::MyModule
# rescue NameError => e
#   puts "NameError: #{ e.message }"
# end
# puts "MyModule.ancestors: #{ MyModule.ancestors.inspect }"

#---------------------------------------------------------------------------------------------------

# ### IRL Example
# require 'active_record'
#
# class Ext
#   # module ActiveRecord (meaning Ext::ActiveRecord) _should have_ been loaded, but was not due to a mistake.
# end
#
# ::ActiveRecord::Base.include(::Ext::ActiveRecord)
# puts ::ActiveRecord::Base.ancestors.inspect
#
# # # Fixed, Ext::ActiveRecord loaded. Ext is a module instead of a class.
# # module Ext
# #   module ActiveRecord; end
# # end
# #
# # ::ActiveRecord::Base.include(::Ext::ActiveRecord)
# # puts ::ActiveRecord::Base.ancestors.inspect
