# ruby transform_keys.rb

# frozen_string_literal: true

require 'digest'

h = { name: "Andy", email: "andy@example.com" }

puts "h = #{ h.inspect }"

transformed_h = h.transform_keys(&:to_s)
puts "h.transform_keys(&:to_s): #{ transformed_h }"

transformed_h = h.transform_keys { |key| Digest::MD5.hexdigest(key.to_s) }
puts "h.transform_keys { |key| Digest::MD5.hexdigest(key.to_s) }: #{ transformed_h }"
