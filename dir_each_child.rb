# ruby dir_each_child.rb

# frozen_string_literal: true

puts "### Dir.entries (< 2.5) ###"
puts Dir.entries(".")

puts "\n### Dir.children (2.5) ###"
puts Dir.children(".")

puts "\n### Dir.each_child (2.5) ###"
puts Dir.each_child(".")
