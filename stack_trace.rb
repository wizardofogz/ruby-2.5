# ASDF_RUBY_VERSION=2.4.2 ruby stack_trace.rb
# ruby stack_trace.rb

# frozen_string_literal: true

module M
  extend self

  def run
    say_something
    fail_miserably
  end

  def say_something
    puts "OK"
  end

  def fail_miserably
    raise "boom"
  end
end

M.run
