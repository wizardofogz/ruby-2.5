# ASDF_RUBY_VERSION=2.4.2 ruby block_rescue.rb

# frozen_string_literal: true

# Ruby < 2.5
instance_eval do
  begin
    raise 'boom'
  rescue
    puts "Something went wrong in a way old Rubies can understand."
  end
end

# Ruby >= 2.5
instance_eval do
  raise 'boom'
rescue
  puts "Something went wrong :("
end

# # Old and new syntaxes side-by-side...sort of.
# OLD_SYNTAX = <<~RUBY
#   instance_eval do
#     begin
#       raise 'boom'
#     rescue
#       puts "Something went wrong in a way old Rubies can understand."
#     end
#   end
# RUBY
#
# NEW_SYNTAX = <<~RUBY
#   instance_eval do
#     raise 'boom'
#   rescue
#     puts "Something went wrong :("
#   end
# RUBY
#
# begin
#   eval(NEW_SYNTAX)
# rescue SyntaxError
#   eval(OLD_SYNTAX)
# end
